FROM debian:bullseye-slim
MAINTAINER https://bitbucket.org/bluesquall/docker-android/issues

# manually make V* man1 *V to prevent openjdk dpkg error
RUN mkdir -p /usr/share/man/man1 \
 && dpkg --add-architecture i386 \
 && apt-get update \
 && apt-get install -y --no-install-recommends \
      build-essential \
      ca-certificates \
      curl \
      gradle \
      libc6:i386 \
      libncurses5:i386 \
      libstdc++6:i386 \
      lib32z1 \
      libbz2-1.0:i386 \
      openjdk-11-jdk \
      unzip \
 && apt-get autoremove -y && apt-get clean

ENV ANDROID_HOME /opt/android
ENV PATH ${PATH}:${ANDROID_HOME}/cmdline-tools:${ANDROID_HOME}/cmdline-tools/bin:${ANDROID_HOME}/platform-tools
ENV CLT_URL https://dl.google.com/android/repository/commandlinetools-linux-9123335_latest.zip
RUN mkdir -p ${ANDROID_HOME}

WORKDIR ${ANDROID_HOME}
RUN curl -SLo sdk.zip ${CLT_URL} \
 && unzip sdk.zip \
 && rm sdk.zip \
 && yes | sdkmanager --licenses --sdk_root=${ANDROID_HOME}

WORKDIR /root
RUN sdkmanager --install \
      'platform-tools' \
    --sdk_root=${ANDROID_HOME}


